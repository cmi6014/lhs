       DOUBLE PRECISION FUNCTION RNUMLHS1()
cc     This function and the next (RNUMLHS2) were added as part of a
cc     refactor to get random number generation working correctly on
cc     the Mac. Previously, RNUMLHS1 & 2 were defined in separate .f90 files.
cc     According to comments there, they were called by:
cc                           ETA,BINOM,CUMULC,CUMULD,ENTRPY,EXPON,     sld01
cc                           GAMMA,GAMMAB,GAMMAM,GEOM,HGEOM,IGAUS,	sld01
cc                           IGAUSF,MIX,NBINOM,NORMAL,PARETO,POISON,    sld01
cc                           TRIANG,UNIFRM,WEIBUL                       sld01
cc Note:  With addition of second random number generator routine       SLD
cc        IGAUSF,GAMMAB,GAMMAM now call RNUMLHS2 instead of RNUMLHS1          SLD
cc     
Cc     For standalone LHS, this remains true.  When LHS is compiled as a 
cc     library for use in Dakota, Dakota exports rnumlhs1 and 2 for use by LHS.
cc     (See packages/pecos/src/BoostRNG_Monostate.hpp.) These functions returns
cc     random numbers produced either by the f90 function DEFAULTRNUM1 or mt13997.
cc     JAS
         DOUBLE PRECISION DEFAULTRNUM1
         RNUMLHS1 = DEFAULTRNUM1() ! defined in DefaultRnum1.f90
         RETURN
       END

       DOUBLE PRECISION FUNCTION RNUMLHS2()
cc     See comments for RNUMLHS1()
         DOUBLE PRECISION DEFAULTRNUM2
         RNUMLHS2 = DEFAULTRNUM2()  ! defined in DefaultRnum2.f90
         RETURN
       END
