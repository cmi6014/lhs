#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv)
{
    int lhsobs = 1000;
    int lhsseed = 1; /* very small values might compromise the randomness; <1000 should be avoided */
    int ierror;

    /*
      LHS_INIT inputs:
      LHSOBS - integer number of observations requested
      LHSSEED - integer random number seed
      LHS_INIT returns:
      IError - integer error flag
      If an error is found, IError = 1 is returned
    */
    /* lhs_init_(&lhsobs, &lhsseed, &ierror); */

    int lnmax = -1;
    int lmaxnnv = -1;
    int lnvar = -1;
    int lnintmx = -1;
    int lncvar = -1;
    int lmaxtb = -1;
    int liprint = -1;
    int lisamw = -1;
    
    lhs_init_mem_(&lhsobs, &lhsseed, &lnmax, &lmaxnnv, &lnvar, &lnintmx, &lncvar, &lmaxtb, &liprint, &lisamw, &ierror);
    printf("lhs_init_mem: %d\n", ierror);

    int lhsreps = 1;
    int lhspval = 1;
    char *lhsopts = ""; /* contain RANDOM SAMPLE, RANDOM PAIRING, none (-> restricted pairing?), both */

    /*
      Integer value input:
      LHSREPS = LHS keyword for multiple number of samples
      LHSPVAL = LHS keyword for selection of point value
      String input:
      LHSOPTS = allows user control of sampling and pairing methods
      This string may contain "RANDOM PAIRING","RANDOM SAMPLE", or both
    */
    lhs_options_(&lhsreps, &lhspval, lhsopts, &ierror, strlen(lhsopts));
    printf("lhs_options: %d\n", ierror);


    /*
      NAMVAR = name of the variable
      IPTFLAG = 0, if PTVAL is NOT to be used
      IPTFLAG = 1, if PTVAL is to be used
      PTVAL = point value associated with NAMVAR
      DISTYPE = character string naming distribution type
      APRAMS =  array of real numbers that are the distribution parameters
      NUMPRMS = number of parameters in array APRAMS
      Outputs:
      IError = error flag, returned = 1 to indicate some error occurred
      IDISTNO = current distribution number
      IPVNO = point value index number
    */

    char *namvar1 = "X1";
    int iptflag = 0;
    double *ptval = NULL;
    char *distype = "NORMAL"; /* page 36--38 */
    int numprms = 2;
    double *aprams = malloc(numprms * sizeof(double));
    aprams[0] = 10.0;
    aprams[1] = 1.0;
    int idistno, ipvno;

    lhs_dist_(namvar1, &iptflag, &ptval, distype, aprams, &numprms, &ierror, &idistno, &ipvno, strlen(namvar1), strlen(distype));
    printf("lhs_dist 1: %d\n", ierror);


    char *namvar2 = "X2";
    distype = "NORMAL";
    aprams[0] = 20.0;
    aprams[1] = 2.0;
    
    lhs_dist_(namvar2, &iptflag, &ptval, distype, aprams, &numprms, &ierror, &idistno, &ipvno, strlen(namvar2), strlen(distype));
    printf("lhs_dist 2: %d\n", ierror);

    /*
      lhs_sdist_(); inputs data for user-defined n-part histogram
      lhs_udist_(); inputs data for user-defined distributions
    */

    /*
      Inputs:
      NAM1 = first variable name, character
      NAM2 = second variable name, character
      CORRVAL = correlation value parameter, real #  between -1 and 1 (strictly)
      Outputs:
      IError = error flag, returned = 1 to indicate some error occurred
    */

    double corrval = 0.999;
    
    lhs_corr_(namvar1, namvar2, &corrval, &ierror, strlen(namvar1), strlen(namvar2));
    printf("lhs_corr: %d\n", ierror);


    /*
      LHS_PREP returns:
      IError = 1, to inidicate some error was found
      NUMNAM - the integer number of variable names including "same as"
      NUMVAR- the number of variables for which observations
      are generated (with no duplication for same as)
    */

    int numnam = 2;
    int numvar = 2;
    
    lhs_prep_(&ierror, &numnam, &numvar);
    printf("lhs_prep: %d\n", ierror);


    /*
      LHS_RUN inputs:
      MAXVAR - Maximum number of variables, integer
      MAXOBS - Maximum number of observation, integer
      MAXNAM - Maximum number of variable names including "same as"
      RFLAG - flag to indicate whether the rank matrix is being passed in 
      LHS_RUN returns:
      LSTDNAM - a list of distribution names (type character)
      INDXNAM - integer array containing index number(position)
      of names in sample data
      PTVALST - an array of the associated point values (type real)
      SMATX - the sample data matrix dimension (MAXVAR,MAXOBS)
      RFLAG - flag to indicate if no rank data needs to be passed (RFLAG=0),
      if rank data will be set (RFLAG = 1),
      if rank data will be obtained (RFLAG = 2), 
      or if rank data will both be "set" and "get" (RFLAG=3) 
      RMATX - the rank data matrix dimension (MAXVAR,MAXOBS)
      NUMNAM - the integer number of variable names including "same as"
      NUMVAR- the number of variables for which observations
      are generated (with no duplication for same as)
      IError - integer error flag
      If an error is found, IError = 1 is returned
    */

    int maxvar = 2;
    int maxobs = 10; //?
    int maxnam = 2;
    int rflag = 0; // ?
    char (*lstname)[16] = malloc(maxvar * sizeof(char[16])); /* SamStor.f90 */
    int *indxnam = malloc(maxnam * sizeof(int));
    double *ptvalst = malloc(maxnam * sizeof(double));
    double *smatx = malloc(maxvar*maxobs * sizeof(double) + 15840);
    double *rmatx = malloc(maxvar*maxobs * sizeof(double));
    
    lhs_run_(&maxvar, &maxobs, &maxnam, &ierror, lstname, indxnam, ptvalst, &numnam, smatx, &numvar, rmatx, &rflag); // output strlen?
    printf("lhs_run: %d\n", ierror);

    for (int j = 0; j < maxobs; ++j) {
	for (int i = 0; i < maxvar; ++i) {
	    printf("%8.3f  ", smatx[j*maxvar + i]);
	}
	printf("\n");
    }

    for (int i = 0; i < maxvar; i++)
	printf("%.16s", lstname[i]);

    printf("\n");
    
    lhs_close_(&ierror);
}
