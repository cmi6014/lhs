cmake_minimum_required(VERSION 3.0)
project(LHS-pokus C Fortran)

# include(FortranCInterface)

### add_subdirectory(LHS EXCLUDE_FROM_ALL)

# make this applicable to gfortran only?
set(FIXED_FLAG "-ffixed-form")
set(STD_LEGACY_FLAG "-std=legacy")

set(LHS_SOURCE_DIR ${CMAKE_SOURCE_DIR}/LHS)

# add_subdirectory(${LHS_SOURCE_DIR}/mods)

# add_subdirectory(mod)
add_library(lhs_mod ${LHS_SOURCE_DIR}/mods/mod/Parms.f90)
target_compile_options(lhs_mod PRIVATE ${FIXED_FLAG} ${FPP_FLAG})
set_property(TARGET lhs_mod PROPERTY POSITION_INDEPENDENT_CODE ON)


# include_directories(${CMAKE_CURRENT_BINARY_DIR}/mod)

## Define the sources for the primary LHS module.
set(LHS_MODS_SRC 
  ${LHS_SOURCE_DIR}/mods/Ccmatr.f90
  ${LHS_SOURCE_DIR}/mods/Cparam.f90
  ${LHS_SOURCE_DIR}/mods/Crank.f90
  ${LHS_SOURCE_DIR}/mods/Csamp.f90
  ${LHS_SOURCE_DIR}/mods/Cworkc.f90
  ${LHS_SOURCE_DIR}/mods/Cworkx.f90
  ${LHS_SOURCE_DIR}/mods/Distnm.f90
  ${LHS_SOURCE_DIR}/mods/Killfile.f90
  ${LHS_SOURCE_DIR}/mods/Miscmod.f90
  ${LHS_SOURCE_DIR}/mods/ModInByC.f90
  )

## Set the convenience library name for the primary LHS modules.
add_library(lhs_mods ${LHS_MODS_SRC})
target_compile_options(lhs_mods PRIVATE ${FIXED_FLAG} ${FPP_FLAG})
set_property(TARGET lhs_mods PROPERTY POSITION_INDEPENDENT_CODE ON)
target_link_libraries(lhs_mods lhs_mod)

set(LHS_UTIL_SRC
  ${LHS_SOURCE_DIR}/Banner.f90
  ${LHS_SOURCE_DIR}/Beta.f90
  ${LHS_SOURCE_DIR}/Betafn.f90
  ${LHS_SOURCE_DIR}/Betaic.f90
  ${LHS_SOURCE_DIR}/Betaln.f90
  ${LHS_SOURCE_DIR}/Binom.f90
  ${LHS_SOURCE_DIR}/Chkdat.f90
  ${LHS_SOURCE_DIR}/Chkdim.f90
  ${LHS_SOURCE_DIR}/Chkemp.f90
  ${LHS_SOURCE_DIR}/Chkstr.f90
  ${LHS_SOURCE_DIR}/Chkzro.f90
  ${LHS_SOURCE_DIR}/Chlsky.f90
  ${LHS_SOURCE_DIR}/Cmcrd.f90
  ${LHS_SOURCE_DIR}/Corcal.f90
  ${LHS_SOURCE_DIR}/Corout.f90
  ${LHS_SOURCE_DIR}/Cumulc.f90
  ${LHS_SOURCE_DIR}/Cumuld.f90
  ${LHS_SOURCE_DIR}/C_wrap.f90
  ${LHS_SOURCE_DIR}/Datout.f90
  ${LHS_SOURCE_DIR}/Datsqz.f90
  ${LHS_SOURCE_DIR}/Dmfsd.f90
  ${LHS_SOURCE_DIR}/Dminmax.f90
  ${LHS_SOURCE_DIR}/Dsinv.f90
  ${LHS_SOURCE_DIR}/Entrpf.f90
  ${LHS_SOURCE_DIR}/Entrpy.f90
  ${LHS_SOURCE_DIR}/Errchk.f90
  ${LHS_SOURCE_DIR}/Errget.f90
  ${LHS_SOURCE_DIR}/Erstgt.f90
  ${LHS_SOURCE_DIR}/Erxset.f90
  ${LHS_SOURCE_DIR}/Expon.f90
  ${LHS_SOURCE_DIR}/Factor.f90
  ${LHS_SOURCE_DIR}/Factr2.f90
  ${LHS_SOURCE_DIR}/Fileoc.f90
  ${LHS_SOURCE_DIR}/Findit.f90
  ${LHS_SOURCE_DIR}/Finvnor.f90
  ${LHS_SOURCE_DIR}/Gammab.f90
  ${LHS_SOURCE_DIR}/Gamma.f90
  ${LHS_SOURCE_DIR}/Gammam.f90
  ${LHS_SOURCE_DIR}/Geom.f90
  ${LHS_SOURCE_DIR}/Hgeom.f90
  ${LHS_SOURCE_DIR}/Histo.f90
  ${LHS_SOURCE_DIR}/Hpsrt.f90
  ${LHS_SOURCE_DIR}/Hstout.f90
  ${LHS_SOURCE_DIR}/Hypgeo.f90
  ${LHS_SOURCE_DIR}/Igaus1.f90
  ${LHS_SOURCE_DIR}/Igaus.f90
  ${LHS_SOURCE_DIR}/Igausf.f90
  ${LHS_SOURCE_DIR}/Imtql2.f90
  ${LHS_SOURCE_DIR}/Interp.f90
  ${LHS_SOURCE_DIR}/Intrpd.f90
  ${LHS_SOURCE_DIR}/Ljust.f90
  ${LHS_SOURCE_DIR}/Matinv.f90
  ${LHS_SOURCE_DIR}/Mix.f90
  ${LHS_SOURCE_DIR}/Nbinom.f90
  ${LHS_SOURCE_DIR}/Normal.f90
  ${LHS_SOURCE_DIR}/Outcrd.f90
  ${LHS_SOURCE_DIR}/Outdat.f90
  ${LHS_SOURCE_DIR}/Pareto.f90
  ${LHS_SOURCE_DIR}/Pmtrx.f90
  ${LHS_SOURCE_DIR}/Poison.f90
  ${LHS_SOURCE_DIR}/Posdef.f90
  ${LHS_SOURCE_DIR}/Ranker.f90
  ${LHS_SOURCE_DIR}/Rierfc1.f90
  ${LHS_SOURCE_DIR}/Rmcnp2.f90
  ${LHS_SOURCE_DIR}/Rmcnp.f90
  ${LHS_SOURCE_DIR}/Rmcnpi2.f90
  ${LHS_SOURCE_DIR}/Rmcnpi.f90
  ${LHS_SOURCE_DIR}/Samout.f90
  ${LHS_SOURCE_DIR}/SamStor.f90
  ${LHS_SOURCE_DIR}/Setdef.f90
  ${LHS_SOURCE_DIR}/Sift.f90
  ${LHS_SOURCE_DIR}/Sspev.f90
  ${LHS_SOURCE_DIR}/Table.f90
  ${LHS_SOURCE_DIR}/Tqlrat.f90
  ${LHS_SOURCE_DIR}/Trbak3.f90
  ${LHS_SOURCE_DIR}/Tred3.f90
  ${LHS_SOURCE_DIR}/Triang.f90
  ${LHS_SOURCE_DIR}/Unifrm.f90
  ${LHS_SOURCE_DIR}/Vif.f90
  ${LHS_SOURCE_DIR}/Weibul.f90
  ${LHS_SOURCE_DIR}/Wrtcrd.f90
  ${LHS_SOURCE_DIR}/Wrtpar.f90
  ${LHS_SOURCE_DIR}/Gumbel.f90
  ${LHS_SOURCE_DIR}/Frechet.f90
  ${LHS_SOURCE_DIR}/Lhssetseed.f90
  ${LHS_SOURCE_DIR}/DefaultRnum1.f90
  ${LHS_SOURCE_DIR}/DefaultRnum2.f90
  )

set(LHS_CORE_SRC
  ${LHS_SOURCE_DIR}/Lhs_cls.f90
  ${LHS_SOURCE_DIR}/Lhs_cnst.f90
  ${LHS_SOURCE_DIR}/Lhs_corr.f90
  ${LHS_SOURCE_DIR}/Lhs_cout.f90
  ${LHS_SOURCE_DIR}/Lhs_dist.f90
  ${LHS_SOURCE_DIR}/Lhs_file.f90
  ${LHS_SOURCE_DIR}/Lhs_imem.f90
  ${LHS_SOURCE_DIR}/Lhs_init.f90
  ${LHS_SOURCE_DIR}/Lhs_optn.f90
  ${LHS_SOURCE_DIR}/Lhs_prep.f90
  ${LHS_SOURCE_DIR}/Lhs_rsed.f90
  ${LHS_SOURCE_DIR}/Lhs_run.f90
  ${LHS_SOURCE_DIR}/Lhs_same.f90
  ${LHS_SOURCE_DIR}/Lhs_sdst.f90
  ${LHS_SOURCE_DIR}/Lhs_udst.f90
  )

# include_directories(${CMAKE_CURRENT_BINARY_DIR}/LHS/mods)
# include_directories(${CMAKE_CURRENT_BINARY_DIR}/LHS/mods/mod)

#include_directories(${LHS_SOURCE_DIR}/mods)
#include_directories(${LHS_SOURCE_DIR}/mods/mod)

# check whether we really need lhsdrv (provide own rnumlhs1,2?)
set(LHSDRV_SRC
  ${LHS_SOURCE_DIR}/fileleng.f90
  ${LHS_SOURCE_DIR}/flname.f90
  ${LHS_SOURCE_DIR}/lhs.f90
  ${LHS_SOURCE_DIR}/lhsdrv.f90
  ${LHS_SOURCE_DIR}/lread.f90
  ${LHS_SOURCE_DIR}/newcrd.f90
  ${LHS_SOURCE_DIR}/rdpar2.f90
  ${LHS_SOURCE_DIR}/rdpar.f90
  ${LHS_SOURCE_DIR}/read.f90
)

add_library(lhs SHARED ${LHS_UTIL_SRC} ${LHS_CORE_SRC} Rnumlhs.f90)
target_compile_options(lhs PRIVATE ${FIXED_FLAG} ${STD_LEGACY_FLAG})
target_link_libraries(lhs lhs_mod lhs_mods)

add_executable(lhsdrv ${LHSDRV_SRC})
target_compile_options(lhsdrv PRIVATE ${FIXED_FLAG} ${STD_LEGACY_FLAG})
target_link_libraries(lhsdrv lhs)

add_executable(lhs-pokus lhs-pokus.c)
target_link_libraries(lhs-pokus lhs)
